import turtle

# win = turtle.Screen()
# win.bgcolor("white")

# tess = turtle.Turtle()

# tess.speed(0)
# tess.color("blue")             
# tess.pensize(5)                 
# offSet=100

# def doNextEvent(x,y):

#     global offSet
#     global win
#     tess.forward(20)
#     tess.left(1+offSet)
#     offSet=offSet-2
#     if(offSet<1):
#         win.exitonclick()


# win.onclick(doNextEvent)
# win.listen()
# win.mainloop()

x = turtle.Turtle()
x.right(90)
x.forward(100)
x.left(90)
x.circle(-100)
# x.listen()
# x.mainloop()

# Python program to demonstrate 
# spiral circle drawing 
	
t = turtle.Turtle() 

# taking radius of initial radius 
r = 10

# Loop for printing spiral circle 
for i in range(100): 
	t.circle(r + i, 45) 

# Python program to demonstrate 
# tangent circle drawing 


t = turtle.Turtle() 

# radius for smallest circle 
r = 10

# number of circles 
n = 10

# loop for printing tangent circles 
for i in range(1, n + 1, 1): 
	t.circle(r * i) 
	t.done()
